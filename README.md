#Cafe info#

Run: "npm start" или "node app.js".
Load data: "mocha loadData" (city, cafe, dish).

##Desctiptions:##
###This app allows:###
* get, add, change and delete city;
* get, add, change and delete category;
* add cafe in the city, get cafes on these cities, change information about cafe, delete cafe;
* add dish in the cafe, get dish on these cafe, change information about dish, delete dish; add pictures to the dish, get dish's pictures, delete dish's pictures;

##API##
###City###
* ####GET####
* /api/city/ - get all cities;
* /api/city/:cityId - get city by id;

* ####POST####
* /api/city/ - add city;

* ####PUT####
* /api/city/:cityId - change city by id;

* ####DELETE####
* /api/city/:cityId - delete city by id;

###Category###
* ####GET####
* /api/category/ - get all categories;
* /api/category/:cityId - get category by id;

* ####POST####
* /api/category/ - add category;

* ####PUT####
* /api/city/:cityId - change city by id;

* ####DELETE####
* /api/category/:categoryId - delete category by id;

###Cafe###
* ####GET####
* /api/cafe/ - get all cafe;
* /api/cafe/:cafeId - get cafe by id;
* /api/cafe/byCityId/:cityId - get all cafe in the city by id;
* /api/cafe/byCityIdWithCity/:cityId - get all cafe in the city by id, with information about the city;

* ####POST####
* /api/cafe/:cityId - add cafe in the city by id;

* ####PUT####
* /api/cafe/:cafeId - change cafe by id;

* ####DELETE####
* /api/cafe/:cafeId - delete cafe by id;

###Dish###
* ####GET####
* /api/dish/ - get all dishes;
* /api/dish/:cafeId - get dish by id;
* /api/dish/byCafeId/:cafeId - get all dishes in the cafe by id;
* /api/dish/byCafeIdWithCafe/:cafeId - get all  dishes in the cafe by id, with information about the cafe;
* /api/dish/byCategory/:categoryId - get all dishes by categoryId;
* /api/dish/byCategoryWithCategory/:categoryId - get all dishes by categoryId, with information about the category;;
* /api/dish/image/:imageId - get dish image by image id;

* ####POST####
* /api/dish/:cafeId - add dish in the city by id;
* /api/dish/image/:dishId - add dish image by dish id;

* ####PUT####
* /api/dish/:dishId - change dish by id;

* ####DELETE####
* /api/dish/:dishId - delete dish by id;
* /api/dish/:dishId/image/:imageId' - delete dish image by dish id and image id;