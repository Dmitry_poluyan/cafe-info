'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var dbConnect = require('./libs/mongoose');
var config = require('./config');
var log = require('./libs/log')(module);
var morgan = require('morgan');

var city = require('./routes/city');
var cafe = require('./routes/cafe');
var dish = require('./routes/dish');
var category = require('./routes/category');

var app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(morgan('dev'));

app.use('/api/city', city);
app.use('/api/cafe', cafe);
app.use('/api/dish', dish);
app.use('/api/category', category);

app.use(function(err, req, res, next){
    if(err.name === 'ValidationError'){
        err.status = 422;
    }
    log.error('%s %d %s', req.method, err.status ? err.status : req.statusCode, err.message);
    return res.status(err.status ? err.status : 500).json({
        nameError: err.name,
        messageError: err.message ? err.message : 'Error',
        validationError: err.errors});
});

app.listen(config.get('port'), function(){
    log.info('sever run ' + config.get('port'));
});
module.exports = app;