'use strict';
var log = require('../libs/log')(module);
var CafeModel = require('../models/cafe');
var _ = require('lodash');

module.exports.addCafe = function (req, res, next) {
    var newCafe = new CafeModel({
        cityId: req.params.cityId,
        name: req.body.name,
        descriptions: {
            adress: req.body.adress,
            email: req.body.email,
            phone: req.body.phone,
            wifi: req.body.wifi
        }
    });
    newCafe.trySave().then(function(){
        log.info('New cafe created');
        return res.status(201).json({status: 'OK', cafe: newCafe});
    }).catch(next);
};

module.exports.getAllCafe = function (req, res, next) {
    CafeModel.find({}).then(function (cafes) {
        return res.json({status: 'OK', cafes: cafes});
    }).catch(next);
};

module.exports.getCafeById = function (req, res, next) {
    CafeModel.findById(req.params.cafeId).then(function (cafe) {
        if (!cafe) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        return res.json({status: 'OK', cafe: cafe});
    }).catch(next);
};

module.exports.updateCafe = function (req, res, next) {
    CafeModel.update(
        {_id: req.params.cafeId},
        {$set: _.pick(req.body, [
            'name',
            'cityId',
            'descriptions.adress',
            'descriptions.email',
            'descriptions.phone',
            'descriptions.wifi']
        )})
        .then(function(modifiedCafe){
            log.info('Cafe updated');
            return res.json({status: 'OK', modifiedCafe: modifiedCafe});
        }).catch(next);
};

module.exports.deleteCafe = function (req, res, next) {
    CafeModel.remove({_id: req.params.cafeId}).then(function (modifiedCafe) {
        log.info('Cafe removed');
        return res.send({status: 'OK', modifiedCafe: modifiedCafe});
    }).catch(next);
};

module.exports.getCafesByCity = function (req, res, next) {
    CafeModel.find({cityId: req.params.cityId}).then(function (cafes) {
        return res.json({status: 'OK', cafes: cafes});
    }).catch(next);
};

module.exports.getCafesByCityWithCity = function (req, res, next) {
    CafeModel.find({cityId: req.params.cityId}).populate('cityId').then(function (cafes) {
        return res.json({status: 'OK', cafes: cafes});
    }).catch(next);
};