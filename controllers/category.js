'use strict';
var log = require('../libs/log')(module);
var CategoryModel = require('../models/category');
var _ = require('lodash');

module.exports.addCategory = function (req, res, next) {
    var newCategory = new CategoryModel({
        name: req.body.name,
        descriptions: req.body.descriptions
    });
    newCategory.trySave().then(function(){
        log.info('New category created');
        return res.status(201).json({status: 'OK', category: newCategory});
    }).catch(next);
};

module.exports.getAllCategory = function (req, res, next) {
    CategoryModel.find({}).then(function (categories) {
        return res.json({status: 'OK', categories: categories});
    }).catch(next);
};

module.exports.categoryById = function (req, res, next) {
    CategoryModel.findById(req.params.categoryId).then(function (category) {
        if (!category) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        return res.json({status: 'OK', category: category});
    }).catch(next);
};

module.exports.updateCategory = function (req, res, next) {
    CategoryModel.update(
        {_id: req.params.categoryId},
        {$set: _.pick(req.body, ['name','descriptions'])})
        .then(function(modifiedCategory){
            log.info('Category updated');
            return res.json({status: 'OK', modifiedCategory: modifiedCategory});
        }).catch(next);
};

module.exports.deleteCategory = function (req, res, next) {
    CategoryModel.remove({_id: req.params.categoryId}).then(function (modifiedCategory) {
        log.info('Category removed');
        return res.send({status: 'OK', modifiedCategory: modifiedCategory});
    }).catch(next);
};