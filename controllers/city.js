'use strict';
var log = require('../libs/log')(module);
var CityModel = require('../models/city');
var _ = require('lodash');

module.exports.addCity = function (req, res, next) {
    var newCity = new CityModel({
        name: req.body.name,
        descriptions: req.body.descriptions
    });
    newCity.trySave().then(function(){
        log.info('New city created');
        return res.status(201).json({status: 'OK', city: newCity});
    }).catch(next);
};

module.exports.getAllCity = function (req, res, next) {
    CityModel.find({}).then(function (cities) {
        return res.json({status: 'OK', cities: cities});
    }).catch(next);
};

module.exports.cityById = function (req, res, next) {
    CityModel.findById(req.params.cityId).then(function (city) {
        if (!city) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        return res.json({status: 'OK', city: city});
    }).catch(next);
};

module.exports.updateCity = function (req, res, next) {
    CityModel.update(
        {_id: req.params.cityId},
        {$set: _.pick(req.body, ['name','descriptions'])})
        .then(function(modifiedCity){
            log.info('City updated');
            return res.json({status: 'OK', modifiedCity: modifiedCity});
        }).catch(next);
};

module.exports.deleteCity = function (req, res, next) {
    CityModel.remove({_id: req.params.cityId}).then(function (modifiedCity) {
        log.info('City removed');
        return res.send({status: 'OK', modifiedCity: modifiedCity});
    }).catch(next);
};