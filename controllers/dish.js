'use strict';
var log = require('../libs/log')(module);
var DishModel = require('../models/dish');
var _ = require('lodash');

var Promise = require('bluebird');

var uploadFile = require('../libs/uploadFile')('directory');

module.exports.createDish = function (req, res, next) {
    var newDish = new DishModel({
        cafeId: req.params.cafeId,
        categoryId: req.body.categoryId,
        name: req.body.name,
        weight: req.body.weight,
        descriptions: req.body.descriptions,
        price: req.body.price
    });
    newDish.trySave().then(function(){
        log.info('New dish created');
        return res.status(201).json({status: 'OK', dish: newDish});
    }).catch(next);
};

module.exports.getAllDish = function (req, res, next) {
    DishModel.find({}).then(function (dishes) {
        return res.json({status: 'OK', dishes: dishes});
    }).catch(next);
};

module.exports.getDishById = function (req, res, next) {
    DishModel.findById(req.params.dishId).then(function (dish) {
        if (!dish) {
            var err = new Error('Not found');
            err.status = 404;
            return Promise.reject(err);
        }
        return res.json({status: 'OK', dish: dish});
    }).catch(next);
};

module.exports.updateDish = function (req, res, next) {
    DishModel.update(
        {_id: req.params.dishId},
        {$set: _.pick(req.body, [
            'categoryId',
            'cafeId',
            'image',
            'name',
            'weight',
            'descriptions',
            'price']
        )})
        .then(function(dish){
            if(dish.nModified === 0){
                var err = new Error('Not Modified');
                err.status = 304;
                return Promise.reject(err);
            }
            log.info('Dish updated');
            return res.json({status: 'OK', modifiedDish: dish});
        }).catch(next);
};

module.exports.deleteDish = function (req, res, next) {
    DishModel.findById(req.params.dishId)
        .then(isExist)
        .then(removeDish)
        .then(removeImagesDish)
        .catch(next);

    function isExist(dish){
        if(!dish){
            var err = new Error('Not found');
            err.status = 404;
            return Promise.reject(err);
        }
        return dish;
    }
    function removeDish(dish){
        return dish.remove().then(function (dish) {
            if(dish.nModified === 0){
                var err = new Error('Not Modified');
                err.status = 304;
                return Promise.reject(err);
            }
            return dish;
        });
    }
    function removeImagesDish(dish){
        if(dish.imageIds.length === 0){
            return res.json({status: 'OK', dish: dish});
        }else{
            return Promise.all( dish.imageIds.map(uploadFile.deleteFile)).then(function () {
                return res.json({status: 'OK', dish: dish});
            });
        }
    }
};

module.exports.getDishesByCafe = function (req, res, next) {
    DishModel.find({cafeId: req.params.cafeId}).then(function (dishes) {
        return res.json({status: 'OK', dishes: dishes});
    }).catch(next);
};

module.exports.getDishesByCafeWithCafe = function (req, res, next) {
    DishModel.find({cafeId: req.params.cafeId}).populate('cafeId').then(function (dishes) {
        return res.json({status: 'OK', dishes: dishes});
    }).catch(next);
};

module.exports.getDishesByCategory = function (req, res, next) {
    DishModel.find({categoryId: req.params.categoryId}).then(function (dishes) {
        return res.json({status: 'OK', dishes: dishes});
    }).catch(next);
};

module.exports.getDishesByCategoryWithCategory = function (req, res, next) {
    DishModel.find({categoryId: req.params.categoryId}).populate('categoryId').then(function (dishes) {
        return res.json({status: 'OK', dishes: dishes});
    }).catch(next);
};

module.exports.updateDishImage = function (req, res, next) {
    DishModel.update(
        {_id: req.params.dishId},
        {$addToSet: {imageIds: req.imageId}}).then(function(dish){
            if(dish.nModified === 0){
                var err = new Error('Not Modified');
                err.status = 304;
                return Promise.reject(err);
            }
            log.info('Dish updated');
            return res.json({status: 'OK', modifiedDish: dish});
        }).catch(next);
};

module.exports.saveImage = function (req, res, next) {
    uploadFile.saveFile(req.file).then(function (imageId) {
        req.imageId = imageId;
        return next();
    }).catch(next);
};

module.exports.getImage = function (req, res, next) {
    uploadFile.getFile(req.params.imageId, res).then(function () {
        return res.end();
    }).catch(next);
};

module.exports.deleteImage = function (req, res, next) {
    DishModel.update(
        {_id: req.params.dishId},
        {$pull: {imageIds: req.params.imageId}})
        .then(isModified)
        .then(deleteDishImage)
        .catch(next);

    function isModified(dish){
        if(dish.nModified === 0){
            var err = new Error('Not Modified');
            err.status = 304;
            return Promise.reject(err);
        }
        return dish;
    }
    function deleteDishImage(dish){
        return uploadFile.deleteFile(req.params.imageId).then(function () {
            return res.json({status: 'OK', modifiedDish: dish});
        });
    }
};