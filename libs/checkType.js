'use strict';
var fs = require('fs');

module.exports.checkOnImageType = function (req, res, next) {
    if(req.file && req.file.mimetype !== 'image/jpeg'){
        var filePath = req.file.path;
        fs.stat(filePath, function (err, stats) {
            if(err || !stats.isFile()){
                err = new Error('Not found');
                err.status = 404;
                return next(err);
            }
            fs.unlink(filePath, function (err) {
                if (err){
                    return next(err);
                }
                console.log('successfully deleted');
                err = new Error('Unsupported Media Type');
                err.status = 415;
                return next(err);
            });
        });
    }
    return next();
};