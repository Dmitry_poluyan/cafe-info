'use strict';
var mongoose    = require('mongoose');
var config = require('../config');
var log = require('../libs/log')(module);

mongoose.Promise = require('bluebird');

mongoose.connect(config.get('db:connection'));
var db = mongoose.connection;

db.on('connected', function () {
    log.info('Mongoose connected to ' + config.get('db:connection'));
});

db.on('open', function () {
    log.info('Mongoose connection open');
});

db.on('error',function (err) {
    log.error('Mongoose connection error: ' + err);
});

db.on('disconnected', function () {
    log.info('Mongoose disconnected');
});

module.exports = db;