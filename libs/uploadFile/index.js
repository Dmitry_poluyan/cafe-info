'use strict';

var DirectoryUpload = require('./directoryUpload.js');
var MongoGridUpload = require('./mongoGridUpload.js');

var strategies = {
    'grid': MongoGridUpload,
    'directory': DirectoryUpload
};

function FileUpload(strategy) {
    return new strategies[strategy]();
}

module.exports = FileUpload;