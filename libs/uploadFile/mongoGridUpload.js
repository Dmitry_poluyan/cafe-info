'use strict';
var mongoose = require('mongoose');
var fs = require('fs');
var Grid = require('gridfs-stream');
var gfs = new Grid(mongoose.connection.db, mongoose.mongo);

var Promise = require('bluebird');
Promise.promisifyAll(fs);

fs.existsAsync = Promise.promisify(
    function exists2(path, exists2callback) {
        fs.exists(path, function callbackWrapper(exists) {
            exists2callback(null, exists);
        });
    });

function UploadInMongoGrid () {

    this.saveFile = function(file) {
        return new Promise(function(resolve, reject) {
            var filePath = file.path;

            fs.existsAsync(filePath)
                .then(isFile)
                .then(readFileAndWriteToMongoGrid)
                .catch(reject);

            function isFile(exist){
                if(!exist){
                    var err = new Error('Not found');
                    err.status = 404;
                    reject(err);
                }
                return filePath;
            }

            function readFileAndWriteToMongoGrid(filePath){
                var fileId = new mongoose.mongo.ObjectID();

                var readStream = fs.createReadStream(filePath);
                readStream.on('error', function(err) {
                    reject(err);
                });

                var wrireStream = gfs.createWriteStream({
                    _id: fileId,
                    filename: file.originalname,
                    mode: 'w',
                    content_type: file.mimetype
                });

                wrireStream.on('error', function(err) {
                    reject(err);
                });
                wrireStream.on('close', function(file) {
                    wrireStream.end();

                    fs.unlinkAsync(filePath).then(function () {
                        resolve(file._id);
                    }).catch(reject);
                });

                readStream.pipe(wrireStream);
            }
        });
    };

    this.getFile = function(fileId, res) {
        return new Promise(function(resolve, reject) {
            var id = gfs.tryParseObjectId(fileId);

            gfs.files.findOne({ _id: id }).then(function (file) {
                if(!file){
                    var err = new Error('Not found');
                    err.status = 404;
                    reject(err);
                }
                res.setHeader('Content-type', file.contentType);

                var readStream = gfs.createReadStream({_id: file._id});

                readStream.on('error', function(err) {
                    reject(err);
                });
                readStream.on('end', function() {
                    resolve({status: 'OK'});
                });
                res.on('close', function () {
                    readStream.destroy();
                });

                readStream.pipe(res);

            }).catch(reject);
        });
    };

    this.deleteFile = function(fileId) {
        return new Promise(function(resolve, reject) {
            var id = gfs.tryParseObjectId(fileId);

            return gfs.exist({_id: id})
                .then(isFound)
                .then(removeFileFromMongoGrid)
                .catch(reject);

            function isFound(found){
                if(!found){
                    var err = new Error('Not found');
                    err.status = 404;
                    return reject(err);
                }
                return id;
            }
            function removeFileFromMongoGrid(id){
                return gfs.remove({_id: id}).then(function () {
                    return resolve({status: 'OK'});
                });
            }
        });
    };
}

module.exports = UploadInMongoGrid;