'use strict';
var app = require('../app');

var chai = require('chai');
var expect = chai.expect;
var assert  = chai.assert ;
var supertest = require('supertest');

var shortId = require('shortid');

var cityId;
var cityName = 'City-' + shortId();
var cityDescriptions = 'Description-' + shortId();

var cafeId;
var cafeName = 'Cafe-' + shortId();
var cafeAdress = 'Adress-' + shortId();
var cafeEmail = shortId() + '@mail.ru';
var cafePhone = '+375297839627';
var cafeWifi = true;

var categoryId;
var categoryName = 'Category-' + shortId();
var categoryDescriptions = 'Description-' + shortId();

var dishId;
var dishName = 'Dish-' + shortId();
var dishWeight = 'Weight-' + shortId();
var dishDescriptions = 'Description-' + shortId();
var dishPrice = 'Price-' + shortId();

describe('Load city', function() {

    it('add city', function (done) {
        supertest(app)
            .post('/api/city/')
            .send({
                name: cityName,
                descriptions: cityDescriptions
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var city = res.body.city;
                cityId = city._id;

                assert.property(city, 'name');
                assert.property(city, 'descriptions');
                assert.property(city, '_id');

                assert.typeOf(city, 'object');

                assert.equal(city.name, cityName);
                assert.equal(city.descriptions, cityDescriptions);

                done();
            });
    });

});

describe('Load cafe', function() {

    it('add cafe in the city by city id', function (done) {
        supertest(app)
            .post('/api/cafe/' + cityId)
            .send({
                name: cafeName,
                adress: cafeAdress,
                email: cafeEmail,
                phone: cafePhone,
                wifi: cafeWifi
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cafe = res.body.cafe;
                cafeId = cafe._id;

                assert.property(cafe, 'name');
                assert.property(cafe, '_id');
                assert.property(cafe, 'descriptions');

                assert.typeOf(cafe, 'object');

                assert.equal(cafe.name, cafeName);
                assert.equal(cafe.descriptions.adress, cafeAdress);
                assert.equal(cafe.descriptions.email, cafeEmail.toLowerCase());
                assert.equal(cafe.descriptions.phone, cafePhone);
                assert.equal(cafe.descriptions.wifi, cafeWifi);

                done();
            });
    });

});

describe('Load category', function() {

    it('add category', function (done) {
        supertest(app)
            .post('/api/category/')
            .send({
                name: categoryName,
                descriptions: categoryDescriptions
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var category = res.body.category;
                categoryId = category._id;

                assert.property(category, 'name');
                assert.property(category, 'descriptions');
                assert.property(category, '_id');

                assert.typeOf(category, 'object');

                assert.equal(category.name, categoryName);
                assert.equal(category.descriptions, categoryDescriptions);

                done();
            });
    });

});

describe('Load dish', function() {

    it('add dish in the cafe by cafe id', function (done) {
        supertest(app)
            .post('/api/dish/' + cafeId)
            .send({
                name: dishName,
                weight: dishWeight,
                descriptions: dishDescriptions,
                price: dishPrice,
                categoryId: categoryId
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dish = res.body.dish;
                dishId = dish._id;

                assert.property(dish, 'cafeId');
                assert.property(dish, 'name');
                assert.property(dish, 'weight');
                assert.property(dish, 'descriptions');
                assert.property(dish, 'price');
                assert.property(dish, '_id');
                assert.property(dish, 'categoryId');

                assert.typeOf(dish, 'object');

                assert.equal(dish.name, dishName);
                assert.equal(dish.weight, dishWeight);
                assert.equal(dish.descriptions, dishDescriptions);
                assert.equal(dish.price, dishPrice);
                assert.equal(dish.categoryId, categoryId);

                done();
            });
    });

    it('add image in the dish', function (done) {
        supertest(app)
            .post('/api/dish/image/' + dishId)
            .attach('file', 'test/Desert.jpg')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedDish = res.body.modifiedDish;

                assert.property(modifiedDish, 'ok');
                assert.property(modifiedDish, 'nModified');
                assert.property(modifiedDish, 'n');

                assert.typeOf(modifiedDish, 'object');

                assert.equal(modifiedDish.ok, '1');
                assert.equal(modifiedDish.nModified, '1');
                assert.equal(modifiedDish.n, '1');

                done();
            });
    });

});