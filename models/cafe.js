'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Cafe = new Schema({
    cityId: {
        required: 'The cityId required',
        type: Schema.Types.ObjectId,
        ref: 'City',
        index: {
            unique: true
        }
    },
    name: {
        type: String,
        required: 'The name required',
        unique: 'The name exists',
        trim: true,
        validate: [/^[a-zA-Z0-9-.]{4,20}$/,
            'You must enter: 4 ~ 20 characters (Latin alphabet, digits, ".", "-")'
        ],
        index: {
            unique: true
        }
    },
    descriptions: {
        adress: {
            type: String,
            trim: true,
            validate: [/^[a-zA-Z0-9-.]{4,20}$/,
                'You must enter: 4 ~ 20 characters (Latin alphabet, digits, ".", "-")'
            ]
        },
        email: {
            type: String,
            lowercase: true,
            trim: true,
            minlength: 3,
            maxlength: 40,
            validate: [/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/,
                'You must enter: 3 ~ 40 characters in the format of email'
            ]
        },
        phone: {
            type: String,
            trim: true,
            validate: [/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
                'You must enter: in the format of phone'
            ]
        },
        wifi: {
            type: Boolean
        }
    }
});

Cafe.plugin(require('mongoose-beautiful-unique-validation'));

var CafeModel = mongoose.model('Cafe', Cafe);

module.exports = CafeModel;