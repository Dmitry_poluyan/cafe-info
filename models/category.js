'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Category = new Schema({
    name: {
        type: String,
        required: 'The name required',
        unique: 'The name exists',
        trim: true,
        index: {
            unique: true
        },
        validate: [/^[a-zA-Z0-9-.]{4,30}$/,
            'You must enter: 4 ~ 30 characters (Latin alphabet, digits, ".", "-")'
        ]
    },
    descriptions:{
        type: String,
        trim: true
    }
});

Category.plugin(require('mongoose-beautiful-unique-validation'));

var CategoryModel = mongoose.model('Category', Category);

module.exports = CategoryModel;