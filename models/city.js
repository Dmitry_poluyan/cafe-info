'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var City = new Schema({
    name: {
        type: String,
        required: 'The name required',
        unique: 'The name exists',
        trim: true,
        index: {
            unique: true
        },
        validate: [/^[a-zA-Z0-9-.]{4,20}$/,
            'You must enter: 4 ~ 20 characters (Latin alphabet, digits, ".", "-")'
        ]
    },
    descriptions:{
        type: String,
        trim: true
    }
});

City.plugin(require('mongoose-beautiful-unique-validation'));

var CityModel = mongoose.model('City', City);

module.exports = CityModel;