'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Dish = new Schema({
    cafeId: {
        type: Schema.Types.ObjectId,
        ref: 'Cafe'
    },
    categoryId: {
        type: Schema.Types.ObjectId,
        ref: 'Cafe'
    },
    name: {
        type: String,
        trim: true,
        required: 'The name required',
        validate: [/^[a-zA-Z0-9-.]{4,30}$/,
            'You must enter: 4 ~ 30 characters (Latin alphabet, digits, ".", "-")'
        ]
    },
    weight: {
        type: String
    },
    descriptions: {
        type: String
    },
    price: {
        type: String
    },
    imageIds: [{
        type: String
    }]
});

Dish.plugin(require('mongoose-beautiful-unique-validation'));

var DishModel = mongoose.model('Dish', Dish);

module.exports = DishModel;