'use strict';
var cafeCtrl = require('../controllers/cafe.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/:cafeId', cafeCtrl.getCafeById);
router.get('/byCityId/:cityId', cafeCtrl.getCafesByCity);
router.get('/byCityIdWithCity/:cityId', cafeCtrl.getCafesByCityWithCity);
router.get('/', cafeCtrl.getAllCafe);

router.post('/:cityId', cafeCtrl.addCafe);

router.put('/:cafeId', cafeCtrl.updateCafe);

router.delete('/:cafeId', cafeCtrl.deleteCafe);

module.exports = router;