'use strict';
var categoryCtrl = require('../controllers/category.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/:categoryId', categoryCtrl.categoryById);
router.get('/', categoryCtrl.getAllCategory);

router.post('/', categoryCtrl.addCategory);

router.put('/:categoryId', categoryCtrl.updateCategory);

router.delete('/:categoryId', categoryCtrl.deleteCategory);

module.exports = router;