'use strict';
var cityCtrl = require('../controllers/city.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/:cityId', cityCtrl.cityById);
router.get('/', cityCtrl.getAllCity);

router.post('/', cityCtrl.addCity);

router.put('/:cityId', cityCtrl.updateCity);

router.delete('/:cityId', cityCtrl.deleteCity);

module.exports = router;