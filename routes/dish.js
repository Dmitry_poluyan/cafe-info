'use strict';
var dishCtrl = require('../controllers/dish.js');
var log = require('../libs/log')(module);
var multer  = require('multer');
var checkType = require('../libs/checkType');
var upload = multer({dest: 'tmprStorage/'});

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/:dishId', dishCtrl.getDishById);
router.get('/byCafeId/:cafeId', dishCtrl.getDishesByCafe);
router.get('/byCafeIdWithCafe/:cafeId', dishCtrl.getDishesByCafeWithCafe);
router.get('/byCategory/:categoryId', dishCtrl.getDishesByCategory);
router.get('/byCategoryWithCategory/:categoryId', dishCtrl.getDishesByCategoryWithCategory);
router.get('/', dishCtrl.getAllDish);
router.get('/image/:imageId', dishCtrl.getImage);

router.post('/:cafeId', dishCtrl.createDish);
router.post('/image/:dishId', upload.single('file'), checkType.checkOnImageType, dishCtrl.saveImage, dishCtrl.updateDishImage);

router.put('/:dishId', dishCtrl.updateDish);

router.delete('/:dishId', dishCtrl.deleteDish);
router.delete('/:dishId/image/:imageId', dishCtrl.deleteImage);

module.exports = router;