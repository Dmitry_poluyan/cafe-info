'use strict';
var app = require('../app');

var chai = require('chai');
var expect = chai.expect;
var assert  = chai.assert ;
var supertest = require('supertest');

var shortId = require('shortid');

var cityId;
var cityName = 'City-' + shortId();
var cityDescriptions = 'Description-' + shortId();

var cafeId;
var cafeName = 'Cafe-' + shortId();
var cafeAdress = 'Adress-' + shortId();
var cafeEmail = shortId() + '@mail.ru';
var cafePhone = '+375297839627';
var cafeWifi = true;

var newCafeName = 'Cafe-' + shortId();
var newCafeAdress = 'Adress-' + shortId();
var newCafeEmail = shortId() + '@mail.ru';
var newCafePhone = '+375297839627';
var newCafeWifi = true;

describe('City for cafe -add', function() {

    it('add city', function (done) {
        supertest(app)
            .post('/api/city/')
            .send({
                name: cityName,
                descriptions: cityDescriptions
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var city = res.body.city;
                cityId = city._id;

                assert.property(city, 'name');
                assert.property(city, 'descriptions');
                assert.property(city, '_id');

                assert.typeOf(city, 'object');

                assert.equal(city.name, cityName);
                assert.equal(city.descriptions, cityDescriptions);

                done();
            });
    });

});

describe('Cafe CRUD', function() {

    it('add cafe in the city by city id', function (done) {
        supertest(app)
            .post('/api/cafe/' + cityId)
            .send({
                name: cafeName,
                adress: cafeAdress,
                email: cafeEmail,
                phone: cafePhone,
                wifi: cafeWifi
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cafe = res.body.cafe;
                cafeId = cafe._id;

                assert.property(cafe, 'name');
                assert.property(cafe, '_id');
                assert.property(cafe, 'descriptions');

                assert.typeOf(cafe, 'object');

                assert.equal(cafe.name, cafeName);
                assert.equal(cafe.descriptions.adress, cafeAdress);
                assert.equal(cafe.descriptions.email, cafeEmail.toLowerCase());
                assert.equal(cafe.descriptions.phone, cafePhone);
                assert.equal(cafe.descriptions.wifi, cafeWifi);

                done();
            });
    });

    it('get all cafe', function (done) {
        supertest(app)
            .get('/api/cafe/')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cafes = res.body.cafes;

                assert.typeOf(cafes, 'array');

                done();
            });
    });

    it('get cafe by id', function (done) {
        supertest(app)
            .get('/api/cafe/' + cafeId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cafe = res.body.cafe;

                assert.property(cafe, 'name');
                assert.property(cafe, '_id');
                assert.property(cafe, 'descriptions');

                assert.typeOf(cafe, 'object');

                assert.equal(cafe.name, cafeName);
                assert.equal(cafe.descriptions.adress, cafeAdress);
                assert.equal(cafe.descriptions.email, cafeEmail.toLowerCase());
                assert.equal(cafe.descriptions.phone, cafePhone);
                assert.equal(cafe.descriptions.wifi, cafeWifi);

                done();
            });
    });

    it('get cafe by city id', function (done) {
        supertest(app)
            .get('/api/cafe/byCityId/' + cityId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cafes = res.body.cafes;

                assert.typeOf(cafes, 'array');

                done();
            });
    });

    it('get cafe by city id with city', function (done) {
        supertest(app)
            .get('/api/cafe/byCityIdWithCity/' + cityId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cafes = res.body.cafes;

                assert.typeOf(cafes, 'array');

                done();
            });
    });

    it('edit cafe by id', function (done) {
        supertest(app)
            .put('/api/cafe/'+ cafeId)
            .send({
                name: newCafeName,
                adress: newCafeAdress,
                email: newCafeEmail,
                phone: newCafePhone,
                wifi: newCafeWifi
            })
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCafe = res.body.modifiedCafe;

                assert.property(modifiedCafe, 'ok');
                assert.property(modifiedCafe, 'n');
                assert.property(modifiedCafe, 'nModified');

                assert.typeOf(modifiedCafe, 'object');

                assert.equal(modifiedCafe.ok, '1');
                assert.equal(modifiedCafe.n, '1');
                assert.equal(modifiedCafe.nModified, '1');

                done();
            });
    });

    it('delete cafe by id', function (done) {
        supertest(app)
            .delete('/api/cafe/' + cafeId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCafe = res.body.modifiedCafe;

                assert.property(modifiedCafe, 'ok');
                assert.property(modifiedCafe, 'n');

                assert.typeOf(modifiedCafe, 'object');

                assert.equal(modifiedCafe.ok, '1');
                assert.equal(modifiedCafe.n, '1');

                done();
            });
    });

});

describe('City for cafe - delete', function() {

    it('delete city by id', function (done) {
        supertest(app)
            .delete('/api/city/' + cityId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCity = res.body.modifiedCity;

                assert.property(modifiedCity, 'ok');
                assert.property(modifiedCity, 'n');

                assert.typeOf(modifiedCity, 'object');

                assert.equal(modifiedCity.ok, '1');
                assert.equal(modifiedCity.n, '1');

                done();
            });
    });

});