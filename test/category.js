'use strict';
var app = require('../app');

var chai = require('chai');
var expect = chai.expect;
var assert  = chai.assert ;
var supertest = require('supertest');

var shortId = require('shortid');

var categoryId;
var categoryName = 'Category-' + shortId();
var categoryDescriptions = 'Description-' + shortId();

var newCategoryName = 'Category-' + shortId();
var newCategoryDescriptions = 'Description-' + shortId();

describe('Category CRUD', function() {

    it('add category', function (done) {
        supertest(app)
            .post('/api/category/')
            .send({
                name: categoryName,
                descriptions: categoryDescriptions
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var category = res.body.category;
                categoryId = category._id;

                assert.property(category, 'name');
                assert.property(category, 'descriptions');
                assert.property(category, '_id');

                assert.typeOf(category, 'object');

                assert.equal(category.name, categoryName);
                assert.equal(category.descriptions, categoryDescriptions);

                done();
            });
    });

    it('get all categories', function (done) {
        supertest(app)
            .get('/api/category/')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var categories = res.body.categories;

                assert.typeOf(categories, 'array');

                done();
            });
    });

    it('get category by id', function (done) {
        supertest(app)
            .get('/api/category/' + categoryId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var category = res.body.category;

                assert.property(category, 'name');
                assert.property(category, 'descriptions');
                assert.property(category, '_id');

                assert.typeOf(category, 'object');

                assert.equal(category.name, categoryName);
                assert.equal(category.descriptions, categoryDescriptions);

                done();
            });
    });

    it('edit category by id', function (done) {
        supertest(app)
            .put('/api/category/'+ categoryId)
            .send({
                name: newCategoryName,
                descriptions: newCategoryDescriptions
            })
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCategory = res.body.modifiedCategory;

                assert.property(modifiedCategory, 'ok');
                assert.property(modifiedCategory, 'n');
                assert.property(modifiedCategory, 'nModified');

                assert.typeOf(modifiedCategory, 'object');

                assert.equal(modifiedCategory.ok, '1');
                assert.equal(modifiedCategory.n, '1');
                assert.equal(modifiedCategory.nModified, '1');

                done();
            });
    });

    it('delete city by id', function (done) {
        supertest(app)
            .delete('/api/category/' + categoryId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCategory = res.body.modifiedCategory;

                assert.property(modifiedCategory, 'ok');
                assert.property(modifiedCategory, 'n');

                assert.typeOf(modifiedCategory, 'object');

                assert.equal(modifiedCategory.ok, '1');
                assert.equal(modifiedCategory.n, '1');

                done();
            });
    });

});