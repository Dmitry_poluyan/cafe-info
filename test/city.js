'use strict';
var app = require('../app');

var chai = require('chai');
var expect = chai.expect;
var assert  = chai.assert ;
var supertest = require('supertest');

var shortId = require('shortid');

var cityId;
var cityName = 'City-' + shortId();
var cityDescriptions = 'Description-' + shortId();

var newCityName = 'City-' + shortId();
var newCityDescriptions = 'Description-' + shortId();

describe('City CRUD', function() {

    it('add city', function (done) {
        supertest(app)
            .post('/api/city/')
            .send({
                name: cityName,
                descriptions: cityDescriptions
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var city = res.body.city;
                cityId = city._id;

                assert.property(city, 'name');
                assert.property(city, 'descriptions');
                assert.property(city, '_id');

                assert.typeOf(city, 'object');

                assert.equal(city.name, cityName);
                assert.equal(city.descriptions, cityDescriptions);

                done();
            });
    });

    it('get all cities', function (done) {
        supertest(app)
            .get('/api/city/')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cities = res.body.cities;

                assert.typeOf(cities, 'array');

                done();
            });
    });

    it('get city by id', function (done) {
        supertest(app)
            .get('/api/city/' + cityId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var city = res.body.city;

                assert.property(city, 'name');
                assert.property(city, 'descriptions');
                assert.property(city, '_id');

                assert.typeOf(city, 'object');

                assert.equal(city.name, cityName);
                assert.equal(city.descriptions, cityDescriptions);

                done();
            });
    });

    it('edit city by id', function (done) {
        supertest(app)
            .put('/api/city/'+ cityId)
            .send({
                name: newCityName,
                descriptions: newCityDescriptions
            })
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCity = res.body.modifiedCity;

                assert.property(modifiedCity, 'ok');
                assert.property(modifiedCity, 'n');
                assert.property(modifiedCity, 'nModified');

                assert.typeOf(modifiedCity, 'object');

                assert.equal(modifiedCity.ok, '1');
                assert.equal(modifiedCity.n, '1');
                assert.equal(modifiedCity.nModified, '1');

                done();
            });
    });

    it('delete city by id', function (done) {
        supertest(app)
            .delete('/api/city/' + cityId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCity = res.body.modifiedCity;

                assert.property(modifiedCity, 'ok');
                assert.property(modifiedCity, 'n');

                assert.typeOf(modifiedCity, 'object');

                assert.equal(modifiedCity.ok, '1');
                assert.equal(modifiedCity.n, '1');

                done();
            });
    });

});