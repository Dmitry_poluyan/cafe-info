'use strict';
var app = require('../app');

var chai = require('chai');
var expect = chai.expect;
var assert  = chai.assert ;
var supertest = require('supertest');

var shortId = require('shortid');

var cityId;
var cityName = 'City-' + shortId();
var cityDescriptions = 'Description-' + shortId();

var cafeId;
var cafeName = 'Cafe-' + shortId();
var cafeAdress = 'Adress-' + shortId();
var cafeEmail = shortId() + '@mail.ru';
var cafePhone = '+375297839627';
var cafeWifi = true;

var categoryId;
var categoryName = 'Category-' + shortId();
var categoryDescriptions = 'Description-' + shortId();

var dishId;
var dishName = 'Dish-' + shortId();
var dishWeight = 'Weight-' + shortId();
var dishDescriptions = 'Description-' + shortId();
var dishPrice = 'Price-' + shortId();

var newDishName = 'Dish-' + shortId();
var newDishWeight = 'Weight-' + shortId();
var newDishDescriptions = 'Description-' + shortId();
var newDishPrice = 'Price-' + shortId();

var imageId;

describe('City for dish - add', function() {

    it('add city', function (done) {
        supertest(app)
            .post('/api/city/')
            .send({
                name: cityName,
                descriptions: cityDescriptions
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var city = res.body.city;
                cityId = city._id;

                assert.property(city, 'name');
                assert.property(city, 'descriptions');
                assert.property(city, '_id');

                assert.typeOf(city, 'object');

                assert.equal(city.name, cityName);
                assert.equal(city.descriptions, cityDescriptions);

                done();
            });
    });

});

describe('Cafe for dish - add', function() {

    it('add cafe in the city by city id', function (done) {
        supertest(app)
            .post('/api/cafe/' + cityId)
            .send({
                name: cafeName,
                adress: cafeAdress,
                email: cafeEmail,
                phone: cafePhone,
                wifi: cafeWifi
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var cafe = res.body.cafe;
                cafeId = cafe._id;

                assert.property(cafe, 'name');
                assert.property(cafe, '_id');
                assert.property(cafe, 'descriptions');

                assert.typeOf(cafe, 'object');

                assert.equal(cafe.name, cafeName);
                assert.equal(cafe.descriptions.adress, cafeAdress);
                assert.equal(cafe.descriptions.email, cafeEmail.toLowerCase());
                assert.equal(cafe.descriptions.phone, cafePhone);
                assert.equal(cafe.descriptions.wifi, cafeWifi);

                done();
            });
    });

});

describe('Category for dish - add', function() {

    it('add category', function (done) {
        supertest(app)
            .post('/api/category/')
            .send({
                name: categoryName,
                descriptions: categoryDescriptions
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var category = res.body.category;
                categoryId = category._id;

                assert.property(category, 'name');
                assert.property(category, 'descriptions');
                assert.property(category, '_id');

                assert.typeOf(category, 'object');

                assert.equal(category.name, categoryName);
                assert.equal(category.descriptions, categoryDescriptions);

                done();
            });
    });

});

describe('Dish CRUD', function() {

    it('add new dish', function (done) {
        supertest(app)
            .post('/api/dish/' + cafeId)
            .send({
                name: dishName,
                weight: dishWeight,
                descriptions: dishDescriptions,
                price: dishPrice,
                categoryId: categoryId
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dish = res.body.dish;
                dishId = dish._id;

                assert.property(dish, 'cafeId');
                assert.property(dish, 'name');
                assert.property(dish, 'weight');
                assert.property(dish, 'descriptions');
                assert.property(dish, 'price');
                assert.property(dish, '_id');
                assert.property(dish, 'categoryId');

                assert.typeOf(dish, 'object');

                assert.equal(dish.name, dishName);
                assert.equal(dish.weight, dishWeight);
                assert.equal(dish.descriptions, dishDescriptions);
                assert.equal(dish.price, dishPrice);
                assert.equal(dish.categoryId, categoryId);

                done();
            });
    });

    it('add image in dish', function (done) {
        supertest(app)
            .post('/api/dish/image/' + dishId)
            .attach('file', 'test/Desert.jpg')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedDish = res.body.modifiedDish;

                assert.property(modifiedDish, 'ok');
                assert.property(modifiedDish, 'nModified');
                assert.property(modifiedDish, 'n');

                assert.typeOf(modifiedDish, 'object');

                assert.equal(modifiedDish.ok, '1');
                assert.equal(modifiedDish.nModified, '1');
                assert.equal(modifiedDish.n, '1');

                done();
            });
    });

    it('get dish by dish id', function (done) {
        supertest(app)
            .get('/api/dish/'+ dishId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dish = res.body.dish;
                imageId = dish.imageIds[0];

                assert.property(dish, 'cafeId');
                assert.property(dish, 'categoryId');
                assert.property(dish, 'name');
                assert.property(dish, 'weight');
                assert.property(dish, '_id');
                assert.property(dish, 'descriptions');
                assert.property(dish, 'price');
                assert.property(dish, 'imageIds');

                assert.typeOf(dish, 'object');
                assert.typeOf(dish.imageIds, 'array');

                assert.equal(dish.cafeId, cafeId);
                assert.equal(dish.name, dishName);
                assert.equal(dish.weight, dishWeight);
                assert.equal(dish.descriptions, dishDescriptions);
                assert.equal(dish.price, dishPrice);
                assert.equal(dish.categoryId, categoryId);

                done();
            });
    });

    it('get all dishes', function (done) {
        supertest(app)
            .get('/api/dish/')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dishes = res.body.dishes;

                assert.typeOf(dishes, 'array');

                done();
            });
    });

    it('get dishes by cafe id', function (done) {
        supertest(app)
            .get('/api/dish/byCafeId/'+ cafeId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dishes = res.body.dishes;

                assert.typeOf(dishes, 'array');

                done();
            });
    });

    it('get dishes by cafe id with cafe', function (done) {
        supertest(app)
            .get('/api/dish/byCafeIdWithCafe/'+ cafeId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dishes = res.body.dishes;

                assert.typeOf(dishes, 'array');

                done();
            });
    });

    it('get dishes by category', function (done) {
        supertest(app)
            .get('/api/dish/byCategory/'+ categoryId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dishes = res.body.dishes;

                assert.typeOf(dishes, 'array');

                done();
            });
    });

    it('get dishes by category with category', function (done) {
        supertest(app)
            .get('/api/dish/byCategoryWithCategory/'+ categoryId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dishes = res.body.dishes;

                assert.typeOf(dishes, 'array');

                done();
            });
    });

    it('edit dish by id', function (done) {
        supertest(app)
            .put('/api/dish/'+ dishId)
            .send({
                name: newDishName,
                weight: newDishWeight,
                descriptions: newDishDescriptions,
                price: newDishPrice,
            })
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedDish = res.body.modifiedDish;

                assert.property(modifiedDish, 'ok');
                assert.property(modifiedDish, 'n');
                assert.property(modifiedDish, 'nModified');

                assert.typeOf(modifiedDish, 'object');

                assert.equal(modifiedDish.ok, '1');
                assert.equal(modifiedDish.n, '1');
                assert.equal(modifiedDish.nModified, '1');

                done();
            });
    });

    it('delete image from dish', function (done) {
        supertest(app)
            .delete('/api/dish/' + dishId + '/image/'+ imageId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedDish = res.body.modifiedDish;

                assert.property(modifiedDish, 'ok');
                assert.property(modifiedDish, 'nModified');
                assert.property(modifiedDish, 'n');

                assert.typeOf(modifiedDish, 'object');

                assert.equal(modifiedDish.ok, '1');
                assert.equal(modifiedDish.nModified, '1');
                assert.equal(modifiedDish.n, '1');

                done();
            });
    });

    it('delete dish by id', function (done) {
        supertest(app)
            .delete('/api/dish/' + dishId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var dish = res.body.dish;

                assert.property(dish, 'categoryId');
                assert.property(dish, 'name');
                assert.property(dish, 'weight');
                assert.property(dish, '_id');
                assert.property(dish, 'descriptions');
                assert.property(dish, 'price');
                assert.property(dish, 'imageIds');

                assert.typeOf(dish, 'object');
                assert.typeOf(dish.imageIds, 'array');

                assert.equal(dish.name, newDishName);
                assert.equal(dish.weight, newDishWeight);
                assert.equal(dish.descriptions, newDishDescriptions);
                assert.equal(dish.price, newDishPrice);

                done();
            });
    });

});

describe('City for dish - delete', function() {

    it('delete city by id', function (done) {
        supertest(app)
            .delete('/api/city/' + cityId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCity = res.body.modifiedCity;

                assert.property(modifiedCity, 'ok');
                assert.property(modifiedCity, 'n');

                assert.typeOf(modifiedCity, 'object');

                assert.equal(modifiedCity.ok, '1');
                assert.equal(modifiedCity.n, '1');

                done();
            });
    });

});

describe('Cafe for dish - delete', function() {

    it('delete cafe by id', function (done) {
        supertest(app)
            .delete('/api/cafe/' + cafeId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCafe = res.body.modifiedCafe;

                assert.property(modifiedCafe, 'ok');
                assert.property(modifiedCafe, 'n');

                assert.typeOf(modifiedCafe, 'object');

                assert.equal(modifiedCafe.ok, '1');
                assert.equal(modifiedCafe.n, '1');

                done();
            });
    });

});

describe('Category for dish - delete', function() {

    it('delete city by id', function (done) {
        supertest(app)
            .delete('/api/category/' + categoryId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var modifiedCategory = res.body.modifiedCategory;

                assert.property(modifiedCategory, 'ok');
                assert.property(modifiedCategory, 'n');

                assert.typeOf(modifiedCategory, 'object');

                assert.equal(modifiedCategory.ok, '1');
                assert.equal(modifiedCategory.n, '1');

                done();
            });
    });

});